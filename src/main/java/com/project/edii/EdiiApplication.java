package com.project.edii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdiiApplication.class, args);
	}

}
