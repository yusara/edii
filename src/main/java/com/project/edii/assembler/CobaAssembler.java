package com.project.edii.assembler;


import com.project.edii.model.dto.CobaDto;
import com.project.edii.model.entity.Coba;
import com.project.edii.repository.CobaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CobaAssembler implements InterfaceAssembler<Coba, CobaDto> {

    @Autowired
    private CobaRepo repository;

    @Override
    public Coba fromDto (CobaDto dto){
        if (dto == null) return null;

        Coba entity = new Coba();
        if(dto.getId() != null){
            Optional<Coba> temp = this.repository.findById(dto.getId());
            if(temp.isPresent()){
                entity = temp.get();
            }
        }

        if (dto.getId() != null) entity.setId(dto.getId());
        if (dto.getNama() != null) entity.setNama(dto.getNama());
        if (dto.getAlamat() != null) entity.setAlamat(dto.getAlamat());

        return entity;
    }

    @Override
    public CobaDto fromEntity(Coba entity){
        if(entity == null) return null;
        return CobaDto.builder()
                .id(entity.getId())
                .nama(entity.getNama())
                .alamat(entity.getAlamat())
                .build();
    }
}
