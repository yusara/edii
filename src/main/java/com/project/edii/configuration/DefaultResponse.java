package com.project.edii.configuration;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class DefaultResponse<T> {
    private int status;
    private String message;
    private T data;


    public DefaultResponse(int status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public static <T> com.project.edii.configuration.DefaultResponse<T> ok(T data){
        return new com.project.edii.configuration.DefaultResponse(HttpStatus.OK.value(), "SUCCESS", data);
    }

    public static <T> com.project.edii.configuration.DefaultResponse<T> ok(T data, String message){
        return new com.project.edii.configuration.DefaultResponse(HttpStatus.OK.value(), message, data);
    }

    public static <T> com.project.edii.configuration.DefaultResponse<T> error(String message){
        return new com.project.edii.configuration.DefaultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, null);
    }
}
