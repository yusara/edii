package com.project.edii.controller;

import com.project.edii.assembler.CobaAssembler;
import com.project.edii.configuration.DefaultResponse;
import com.project.edii.model.dto.CobaDto;
import com.project.edii.model.entity.Coba;
import com.project.edii.repository.CobaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/coba")
public class CobaController {
    @Autowired
    private CobaRepo cobaRepo;

    @Autowired
    private CobaAssembler assembler;

    @GetMapping
    public DefaultResponse getAll(){
        List<Coba> cobaList = cobaRepo.findAll();
        List<CobaDto> cobaDtos = cobaList.stream().map(coba -> assembler.fromEntity(coba)).collect(Collectors.toList());
        return DefaultResponse.ok(cobaDtos);
    }

    @GetMapping("/{id}")
    public DefaultResponse getId(@PathVariable Integer id){
        CobaDto cobaDtos = assembler.fromEntity(cobaRepo.findById(id).get());
        return DefaultResponse.ok(cobaDtos);
    }

    @PostMapping
    public DefaultResponse insert(@RequestBody CobaDto dto) {
        Coba coba = assembler.fromDto(dto);
        cobaRepo.save(coba);
        return DefaultResponse.ok(dto);
    }

    @PutMapping
    public DefaultResponse update(@RequestBody CobaDto dto){
        Coba coba = assembler.fromDto(dto);
        cobaRepo.save(coba);
        return DefaultResponse.ok(dto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        cobaRepo.deleteById(id);
    }

}
