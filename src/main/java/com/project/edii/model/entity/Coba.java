package com.project.edii.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_coba")
public class Coba {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column (name = "id")
    private Integer id;

    @Column (name = "nama")
    private String nama;

    @Column (name = "alamat")
    private String alamat;
}
