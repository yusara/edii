package com.project.edii.repository;

import com.project.edii.model.entity.Coba;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CobaRepo extends JpaRepository<Coba, Integer> {

}
