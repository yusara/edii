$(document).ready(function() {

	var tableBiodata = {
		create: function () {
			// jika table tersebut datatable, maka clear and dostroy
			if ($.fn.DataTable.isDataTable('#tableBiodata')) {
				//table yg sudah dibentuk menjadi datatable harus d rebuild lagi untuk di instantiasi ulang
				$('#tableBiodata').DataTable().clear();
				$('#tableBiodata').DataTable().destroy();
			}

			$.ajax({
				// url: 'coba/'+$("#search").val(),
				url: 'coba/',
				method: 'get',
				contentType: 'application/json',
				success: function (res, status, xhr) {
					console.log('person/'+$("#search").val());
					if (xhr.status == 200 || xhr.status == 201) {
						$('#tableBiodata').DataTable({
							data: [res[0].data],
							columns: [
								{
									title: "Id",
									data: "id"
								},

								{
									title: "Nama",
									data: "nama"
								},

								{
									title: "Alamat",
									data: "alamat"
								},

								{
									title: "Action",
									data: null,
									render: function (data, type, row) {
										return "<button class='btn-primary' onclick=formBiodata.setEditData('" + data.id + "')>Edit</button>"
									}
								}
							]
						});

					} else {

					}
				},
				error: function (err) {
					console.log(err);
				}
			});


		}
	}


	$("#btn-simpan").click(function () {
		console.log("tombol save ditekan");
		insertData.saveForm();
	});


	tableBiodata.create();

	console.log('test');
	// alert("halo");

	var insertData = {
		saveForm: function () {
			// if ($('#form-data').parsley().validate()) {
				console.log("Berhasil masuk function saveform");
				var dataResult = getJsonForm($("#form-data").serializeArray(), true);
				console.log(dataResult);
				$.ajax({
					url: 'coba/',
					method: 'post',
					contentType: 'application/json',
					dataType: 'json',
					data: JSON.stringify(dataResult),
					success: function (res, status, xhr) {
						// tableData.create();
						console.log("berhasil kirim data");
					},
					error: function (err) {
						console.log(err);
					}
				});
			// }
		}
	}






});